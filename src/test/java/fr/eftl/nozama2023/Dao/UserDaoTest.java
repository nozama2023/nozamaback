package fr.eftl.nozama2023.Dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import fr.eftl.nozama2023.dao.UserDao;
import fr.eftl.nozama2023.model.User;



@ExtendWith(SpringExtension.class)
@SpringBootTest
@WebAppConfiguration
@Transactional
@ContextConfiguration
@DirtiesContext
@TestMethodOrder(OrderAnnotation.class)
public class UserDaoTest {
@Autowired
    
    private UserDao userDao;
    @Test
    @org.junit.jupiter.api.Order(1)
    void testGetUserById() {
        // Given
        int userId = 1;

        // When
        Optional<User> userOptional = userDao.findById(userId);

        System.out.println("Is user present? "+ userOptional.isPresent());
        // Then
        assertTrue(userOptional.isPresent(), "Utilisateur non trouvé");
        User user = userOptional.get();
        assertEquals(userId, user.getId(), "L'ID de l'utilisateur doit être le même que l'entrée");
    }

    @Test
    @org.junit.jupiter.api.Order(2)
    void testGetUserByLastname() {
        // Given
        String userName = "User1";

        // When
        List<User> users = userDao.findByLastname(userName);

        // Then
        assertTrue(users.isEmpty(), "Pas d'utilisateur trouvé avec ce nom");
        for (User user : users) {
            assertEquals(userName, user.getLastname(), "Le nom de l'utilisateur est bon");
        }
    } 

    @Test
    @org.junit.jupiter.api.Order(3)
    void testGetUserByFirstname() {
    	
    	String userFirstname = "User2";
        // When
        List<User> users = userDao.findByFirstname(userFirstname);

        // Then
        assertTrue(users.isEmpty(), "Pas d'utilisateur pour ce Prénom");
        for (User user : users) {
            assertEquals(userFirstname, user.getFirstname(), "Le Prénom de l'utilisateur a été trouvée");
        }
    }
    @Test
    @org.junit.jupiter.api.Order(3)
    void testGetUserByMail() {
    	
    	String userMail = "User3";
        // When
        List<User> users = userDao.findByMail(userMail);

        // Then
        assertTrue(users.isEmpty(), "Pas d'utilisateur pour cette adresse mail");
        for (User user : users) {
            assertEquals(userMail, user.getMail(), "Le mail de l'utilisateur a été trouvée");
        }
    }
}
