package fr.eftl.nozama2023.Dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;


import fr.eftl.nozama2023.dao.OrderDao;
import fr.eftl.nozama2023.model.Order;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@WebAppConfiguration
@Transactional
@ContextConfiguration
@DirtiesContext
@TestMethodOrder(OrderAnnotation.class)
public class OrderDaoTest {
@Autowired
    
    private OrderDao orderDao;
    @Test
    @org.junit.jupiter.api.Order(1)
    void testGetOrderById() {
        // Given
        int orderId = 1;

        // When
        Optional<Order> orderOptional = orderDao.findById(orderId);

        System.out.println("Is order present? "+ orderOptional.isPresent());
        // Then
        assertTrue(orderOptional.isPresent(), "Commande non trouvé");
        Order order = orderOptional.get();
        assertEquals(orderId, order.getId(), "L'ID de la commande doit être le même que l'entrée");
    }

    @Test
    @org.junit.jupiter.api.Order(2)
    void testGetOrderByStatus() {
        // Given
        String orderStatus = "Order1";

        // When
        List<Order> orders = orderDao.findByStatus(orderStatus);

        // Then
        assertTrue(orders.isEmpty(), "Pas de commande trouvé avec ce nom");
        for (Order order : orders) {
            assertEquals(orderStatus, order.getStatus(), "Le statut de la commande est bon");
        }
    } 

    @Test
    @org.junit.jupiter.api.Order(3)
    void testGetOrderByDate() {
    	
    	String orderDate = "2000-04-21";
        // When
        List<Order> orders = orderDao.findByDate(orderDate);

        // Then
        assertTrue(orders.isEmpty(), "Pas de commande pour cette date");
        for (Order order : orders) {
            assertEquals(orderDate, order.getDate(), "La date de la commande a été trouvée");
        }
    }

}