package fr.eftl.nozama2023.Dao;

import static org.junit.jupiter.api.Assertions.assertEquals; 
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import org.hibernate.Hibernate;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import fr.eftl.nozama2023.dao.ProductDao;
import fr.eftl.nozama2023.model.Category;
import fr.eftl.nozama2023.model.Product;

// Utilisation de certaines annotations pour les tests
@ExtendWith(SpringExtension.class) // Intégration de Spring pour les tests
@SpringBootTest // Indique que c'est un test Spring Boot
@WebAppConfiguration // Configuration du test pour un environnement web
@Transactional // Transaction pour les tests (rollback à la fin de chaque test)
@ContextConfiguration // Configuration du contexte pour les tests
@DirtiesContext // Nettoie le contexte après chaque test
@TestMethodOrder(OrderAnnotation.class) // Ordre d'exécution des tests en fonction des annotations @Order
public class ProductDaoTest {
    
    @Autowired // Injection de dépendance : Le DAO pour les produits
    private ProductDao productDao;
    
    // Catégorie de test utilisée dans plusieurs tests
    private Category category1 = new Category(1, "NouvelleCategory", "NouvelleDescription");

    @Test
    @Order(1) // Spécifie l'ordre d'exécution des tests
    void testGetProductById() {
        // Given : Données de test (ID du produit à rechercher)
        int productId = 1;

        // When : Exécution du test, recherche du produit par ID dans la base de données
        Optional<Product> productOptional = productDao.findById(productId);

        // Then : Vérification des résultats du test
        // Le produit doit être trouvé dans la base de données et son ID doit correspondre à celui donné
        assertTrue(productOptional.isPresent(), "Produit non trouvé");
        Product product = productOptional.get();
        assertEquals(productId, product.getId(), "L'ID du produit doit être le même que l'entrée");
    }

    @Test
    @Order(2)
    void testGetProductByName() {
        // Given : Données de test (nom du produit à rechercher)
        String productName = "Prod1";

        // When : Exécution du test, recherche des produits par nom dans la base de données
        List<Product> products = productDao.findByName(productName);

        // Then : Vérification des résultats du test
        // Au moins un produit doit être trouvé dans la base de données avec le nom donné
        assertFalse(products.isEmpty(), "Pas de produits trouvé avec ce nom");
        for (Product product : products) {
            assertEquals(productName, product.getName(), "Le nom du produit est bon");
        }
    } 

    @Test
    @Order(3)
    void testGetProductByCategory() {
        // Given : Données de test (catégorie de produits à rechercher)
        Category expectedCategory = new Category();
        expectedCategory.setId(1);
        expectedCategory.setName("Droides");
        expectedCategory.setDescription("Les droides"); // Assurez-vous d'ajuster la description selon votre cas réel

        // When : Exécution du test, recherche des produits par catégorie dans la base de données
        List<Product> products = productDao.findByCategory(category1);

        // Then : Vérification des résultats du test
        // Au moins un produit doit être trouvé dans la base de données avec la catégorie donnée
        assertFalse(products.isEmpty(), "Pas de produits pour cette catégorie");
        for (Product product : products) {
            // Initialiser complètement la catégorie avant de fermer la session
            Hibernate.initialize(product.getCategory());
            
            Category actualCategory = product.getCategory();
            // La catégorie du produit trouvé doit correspondre à la catégorie attendue
            assertEquals(expectedCategory.getId(), actualCategory.getId(), "ID de catégorie incorrect");
            assertEquals(expectedCategory.getName(), actualCategory.getName(), "Nom de catégorie incorrect");
            assertEquals(expectedCategory.getDescription(), actualCategory.getDescription(), "Description de catégorie incorrecte");
        }
    }

    @Test
    @Order(4)
    void testGetProductByReference() {
        // Given : Données de test (référence du produit à rechercher)
        String productReference = "R1";

        // When : Exécution du test, recherche du produit par référence dans la base de données
        Product product = productDao.findByReference(productReference);

        // Then : Vérification des résultats du test
        // Le produit doit être trouvé dans la base de données et sa référence doit correspondre à celle donnée
        assertNotNull(product, "Produit introuvable");
        assertEquals(productReference, product.getReference(), "La référence a été trouvée");
    }

    @Test
    @Order(5)
    void testCreateProduct() {
        // Given : Données de test (nouveau produit à créer avec sa catégorie associée)
        Product newProduct = new Product();
        newProduct.setReference("NEW-REF");
        newProduct.setName("Nouveau produit");
        newProduct.setDescription("une description random");
        newProduct.setQuantity(100);
        newProduct.setPrice(19.99);        
        newProduct.setCategory(category1);

        // When : Exécution du test, création du nouveau produit dans la base de données
        Product createdProduct = productDao.save(newProduct);

        // Then : Vérification des résultats du test
        // Le produit créé ne doit pas être null, et son ID et sa référence doivent être corrects
        assertNotNull(createdProduct, "Produit non créé");
        assertNotNull(createdProduct.getId(), "L'id produit n'a pas été généré");
        assertEquals(newProduct.getReference(), createdProduct.getReference(), "La référence produit a été trouvée");
    }

    @Test
    @Order(6)
    void testUpdateProduct() {
        // Given : Données de test (ID du produit à mettre à jour et nouvelle catégorie associée)
        int productId = 1;
        Category newCategory = new Category();
        newCategory.setId(1);
        newCategory.setName("Updated Category");
        newCategory.setDescription("NouvelleDescription"); // Assurez-vous d'ajuster la description selon votre cas réel

        // When : Exécution du test, mise à jour du produit dans la base de données
        Optional<Product> productOptional = productDao.findById(productId);
        assertTrue(productOptional.isPresent(), "Produit introuvable");
        Product product = productOptional.get();
        Hibernate.initialize(product.getCategory()); // Initialiser la catégorie avant de fermer la session
        product.setCategory(newCategory);
        productDao.save(product);

        // Then : Vérification des résultats du test
        // Le produit mis à jour doit être retrouvé dans la base de données avec sa nouvelle catégorie associée
        Optional<Product> updatedProductOptional = productDao.findById(productId);
        assertTrue(updatedProductOptional.isPresent(), "Le produit n'a pas été mis à jour");
        Product updatedProduct = updatedProductOptional.get();
        Category actualCategory = updatedProduct.getCategory();
        assertEquals(newCategory.getId(), actualCategory.getId(), "ID de catégorie incorrect");
        assertEquals(newCategory.getName(), actualCategory.getName(), "Nom de catégorie incorrect");
        assertEquals(newCategory.getDescription(), actualCategory.getDescription(), "Description de catégorie incorrecte");
    }

    @Test
    @Order(7)
    void testDeleteProduct() {
        // Given : Données de test (ID du produit à supprimer)
        int productId = 2;

        // When : Exécution du test, suppression du produit de la base de données
        Optional<Product> productOptional = productDao.findById(productId);
        assertTrue(productOptional.isPresent(), "Produit introuvable");
        Product product = productOptional.get();
        productDao.delete(product);

        // Then : Vérification des résultats du test
        // Le produit supprimé ne doit pas être retrouvé dans la base de données
        Optional<Product> deletedProductOptional = productDao.findById(productId);
        assertFalse(deletedProductOptional.isPresent(), "Produit pas supprimé");
    }

    
}
