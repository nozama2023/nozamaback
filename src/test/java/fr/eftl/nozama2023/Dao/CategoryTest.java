package fr.eftl.nozama2023.Dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import fr.eftl.nozama2023.dao.CategoryDao;
import fr.eftl.nozama2023.model.Category;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@WebAppConfiguration
@Transactional
@ContextConfiguration
@DirtiesContext
@TestMethodOrder(OrderAnnotation.class)
public class CategoryTest {
@Autowired
    
    private CategoryDao categoryDao;
    @Test
    @org.junit.jupiter.api.Order(1)
    void testGetCategoryById() {
        // Given
        int categoryId = 1;

        // When
        Optional<Category> categoryOptional = categoryDao.findById(categoryId);

        System.out.println("Is category present? "+ categoryOptional.isPresent());
        // Then
        assertTrue(categoryOptional.isPresent(), "Categorie non trouvé");
        Category category = categoryOptional.get();
        assertEquals(categoryId, category.getId(), "L'ID de la categorie doit être le même que l'entrée");
    }

    @Test
    @org.junit.jupiter.api.Order(2)
    void testGetCategoryByName() {
        // Given
        String categoryName = "Category1";

        // When
        List<Category> categories = categoryDao.findByName(categoryName);

        // Then
        assertTrue(categories.isEmpty(), "Pas de categorie trouvé avec ce nom");
        for (Category category : categories) {
            assertEquals(categoryName, category.getName(), "Le nom de la categorie est bon");
        }
    }
}
