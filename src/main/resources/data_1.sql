-- ===============================
-- DDL : Data definition language
-- ex: CREATE, ALTER, DROP, etc.
-- ===============================

CREATE TABLE `category` (
  `Category_id` int NOT NULL AUTO_INCREMENT,
  `Category_name` varchar(255) NOT NULL,
  `Category_description` varchar(255) NOT NULL,
  PRIMARY KEY (`Category_id`)
);

CREATE TABLE `product` (
  `Product_id` INT NOT NULL AUTO_INCREMENT,
  `Product_reference` varchar(255) NOT NULL,
  `Product_name` varchar(255) NOT NULL,
  `Product_description` varchar(255) NOT NULL,
  `Product_quantity` int NOT NULL,
  `Product_price` float NOT NULL,
  `Category_id` int NOT NULL, -- Remplacer "Product_Category_FK" par "Category_id"
  PRIMARY KEY (`Product_id`),
  FOREIGN KEY (`Category_id`) REFERENCES `category`(`Category_id`)
);

CREATE TABLE `grant`(
	`Grant_id` int NOT NULL AUTO_INCREMENT,
	`Grant_name` varchar(255) NOT NULL,
	PRIMARY KEY (`Grant_id`)
);

CREATE TABLE `role` (
  `Role_id` int NOT NULL AUTO_INCREMENT,
  `Role_name` text NOT NULL,
  PRIMARY KEY (`Role_id`)
);

CREATE TABLE `users` (
  `User_id` int NOT NULL AUTO_INCREMENT,
  `User_name` varchar(255) NOT NULL,
  `User_firstname` varchar(255) NOT NULL,
  `User_gender` varchar(255) NOT NULL,
  `User_mobile` int NOT NULL,
  `User_mail` varchar(255) NOT NULL,
  `User_birth_date` date NOT NULL,
  `User_password` varchar(255) NOT NULL,
  `User_sign_in_date` date NOT NULL,
  `Role_id` int NOT NULL,
  `Grant_id` int NOT NULL,
  `Grants` varchar(255),
  PRIMARY KEY (`User_id`),
  FOREIGN KEY (`Role_id`) REFERENCES `role`(`Role_id`),
  FOREIGN KEY (`Grant_id`) REFERENCES `grant`(`Grant_id`)
);

CREATE TABLE `orders` (
  `Order_id` int NOT NULL AUTO_INCREMENT,
  `Order_status` varchar(255) NOT NULL,
  `Order_date` date,
  `Order_price` double NOT NULL,
  `User_id` int NOT NULL,
  PRIMARY KEY (`Order_id`),
  FOREIGN KEY (`User_id`) REFERENCES `users`(`User_id`)
);

INSERT INTO `category` (`Category_name`, `Category_description`) VALUES ('Droides', 'Les droides');
INSERT INTO `category` (`Category_name`, `Category_description`) VALUES ('Categorie 2', 'La categorie numéro 2');

INSERT INTO `product` (`Product_reference`, `Product_name`, `Product_description`, `Product_quantity`, `Product_price`, `Category_id`) VALUES ('R1', 'Prod1', 'Produit numero 1', '2', '150', '1');
INSERT INTO `product` (`Product_reference`, `Product_name`, `Product_description`, `Product_quantity`, `Product_price`, `Category_id`) VALUES ('R2', 'D2', 'Droids d Anakin', '2', '150', '2');
INSERT INTO `product` (`Product_reference`, `Product_name`, `Product_description`, `Product_quantity`, `Product_price`, `Category_id`) VALUES ('R3', 'Prod3', 'Produit numero 3', '2', '150', '1');
INSERT INTO `product` (`Product_reference`, `Product_name`, `Product_description`, `Product_quantity`, `Product_price`, `Category_id`) VALUES ('R4', 'P17', 'Droide de Obi-Wan', '2', '150', '2');

INSERT INTO `grant` (`Grant_name`) VALUES ('JeSaisPas');

INSERT INTO `role`(`Role_name`) VALUES ('Admin');

INSERT INTO `users` (`User_name`, `User_firstname`, `User_gender`, `User_mobile`, `User_mail`, `User_birth_date`, `User_password`, `User_sign_in_date`, `Role_id`, `Grant_id`, `Grants`) VALUES ('Wick', 'Jhon', 'homme', '0675849706', 'jhonwick@gmail.com', '2000-06-12', 'azerty', '2005-06-12', '1', '1', 'test'); 
INSERT INTO `users` (`User_name`, `User_firstname`, `User_gender`, `User_mobile`, `User_mail`, `User_birth_date`, `User_password`, `User_sign_in_date`, `Role_id`, `Grant_id`, `Grants`) VALUES ('Wiliams', 'Robin', 'homme', '0675849706', 'jhonwick@gmail.com', '2000-06-12', 'azerty', '2005-06-12', '1', '1', 'test');

INSERT INTO `orders` (`Order_status`, `Order_date`, `Order_price`, `User_id`) VALUES ('Actif', '2023-06-12', '540', '1');
INSERT INTO `orders` (`Order_status`, `Order_date`, `Order_price`, `User_id`) VALUES ('Actif', '2023-06-12', '645', '2');
INSERT INTO `orders` (`Order_status`, `Order_date`, `Order_price`, `User_id`) VALUES ('Actif', '2023-06-12', '234', '1');

