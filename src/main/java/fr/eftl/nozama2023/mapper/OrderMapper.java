package fr.eftl.nozama2023.mapper;

import fr.eftl.nozama2023.dto.OrderDto;
import fr.eftl.nozama2023.model.Order;

public class OrderMapper {

    public OrderDto mapOrderToOrderDto(Order order) {
        OrderDto orderDto = new OrderDto();
        orderDto.setId(order.getId());
        orderDto.setStatus(order.getStatus());
        orderDto.setDate(order.getDate());
        orderDto.setPrice(order.getPrice());
        return orderDto;
    }

    public Order mapOrderDtoToOrder(OrderDto orderDto) {
        Order order = new Order();
        order.setId(orderDto.getId());
        order.setStatus(orderDto.getStatus());
        order.setDate(orderDto.getDate());
        order.setPrice(orderDto.getPrice());
        return order;
    }

}
