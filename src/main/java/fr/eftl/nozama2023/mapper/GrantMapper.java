package fr.eftl.nozama2023.mapper;

import fr.eftl.nozama2023.dto.GrantDto;
import fr.eftl.nozama2023.model.Grant;

public class GrantMapper {
	
	
		public GrantDto mapGrantToGrantDto(Grant grant) {
			GrantDto grantDto = new GrantDto();
			grantDto.setId(grant.getId());
			grantDto.setName(grant.getName());
			return grantDto;
		}
		
		public Grant mapGrantDtoToGrant(GrantDto grantDto) {
			Grant grant = new Grant();
			grant.setId(grantDto.getId());
			grant.setName(grantDto.getName());
			return grant;
		}

}
