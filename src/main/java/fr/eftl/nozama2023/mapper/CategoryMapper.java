package fr.eftl.nozama2023.mapper;

import fr.eftl.nozama2023.dto.CategoryDto; 
import fr.eftl.nozama2023.model.Category;

public class CategoryMapper {
	
	public CategoryDto mapCategoryToCategoryDto(Category category) {
		CategoryDto categoryDto = new CategoryDto();
		categoryDto.setId(category.getId());
		categoryDto.setName(category.getName());
		categoryDto.setDescription(category.getDescription());
		return categoryDto;
	}
	
	public Category mapCategoryDtoToCategory(CategoryDto categoryDto) {
		Category category = new Category();
		category.setId(categoryDto.getId());
		category.setName(categoryDto.getName());
		category.setDescription(categoryDto.getDescription());
		return category;
	}
	
}

