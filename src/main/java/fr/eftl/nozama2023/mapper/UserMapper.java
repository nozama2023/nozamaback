package fr.eftl.nozama2023.mapper;

import fr.eftl.nozama2023.dto.ProductDto;
import fr.eftl.nozama2023.dto.UserDto;
import fr.eftl.nozama2023.model.Product;
import fr.eftl.nozama2023.model.User;

public class UserMapper {
	public UserDto mapUserToUserDto(User user) {
		UserDto userDto = new UserDto();
		userDto.setId(user.getId());
		userDto.setLastname(user.getLastname());
		userDto.setFirstname(user.getFirstname());
		userDto.setGender(user.getGender());
		userDto.setPhone(user.getPhone());
		userDto.setMail(user.getMail());
		userDto.setBirthdate(user.getBirthdate());
		userDto.setPassword(user.getPassword());
		userDto.setRoleFk(user.getRole());
		userDto.setSigninDate(user.getSigninDate());
		return userDto;
	}
	
	public User mapUserDtoToUser(UserDto userDto) {
		User user = new User();
		user.setId(userDto.getId());
		user.setLastname(userDto.getLastname());
		user.setFirstname(userDto.getFirstname());
		user.setGender(userDto.getGender());
		user.setPhone(userDto.getPhone());
		user.setMail(userDto.getMail());
		user.setBirthdate(userDto.getBirthdate());
		user.setPassword(userDto.getPassword());
		user.setRole(userDto.getRoleFk());
		user.setSigninDate(userDto.getSigninDate());
		return user;
	}
}
