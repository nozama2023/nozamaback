package fr.eftl.nozama2023.mapper;

import fr.eftl.nozama2023.dto.ProductDto; 

import fr.eftl.nozama2023.model.Product;


public class ProductMapper {
	
	public ProductDto mapProductToProductDto(Product product) {
		ProductDto productDto = new ProductDto();
		productDto.setId(product.getId());
		productDto.setName(product.getName());
		productDto.setReference(product.getReference());
		productDto.setDescription(product.getDescription());
		productDto.setQuantity(product.getQuantity());
		productDto.setPrice(product.getPrice());
		productDto.setCategory(product.getCategory());
		return productDto;
	}
	
	public Product mapProductDtoToProduct(ProductDto productDto) {
		Product product = new Product();
		product.setId(productDto.getId());
		product.setName(productDto.getName());
		product.setReference(productDto.getReference());
		product.setDescription(productDto.getDescription());
		product.setQuantity(productDto.getQuantity());
		product.setPrice(productDto.getPrice());
		product.setPrice(productDto.getPrice());
		product.setCategory(productDto.getCategory());
		return product;
	}

}
