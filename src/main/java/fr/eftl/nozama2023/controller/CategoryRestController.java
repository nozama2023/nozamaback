package fr.eftl.nozama2023.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import fr.eftl.nozama2023.dto.CategoryDto;
import fr.eftl.nozama2023.mapper.CategoryMapper;
import fr.eftl.nozama2023.model.Category;
import fr.eftl.nozama2023.service.CategoryService;

@RestController
public class CategoryRestController {

    @Autowired
    private CategoryService categoryService;
    
    private CategoryMapper categoryMapper = new CategoryMapper();

    @GetMapping(value = "/categories", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CategoryDto>> getCategories() {
        List<Category> categories = categoryService.getCategory();
        List<CategoryDto> categoryDtos = new ArrayList<>();
        for (Category category : categories) {
            categoryDtos.add(categoryMapper.mapCategoryToCategoryDto(category));
        }
        return ResponseEntity.ok(categoryDtos);
    }

    @GetMapping("/categories/{id}")
    public ResponseEntity<CategoryDto> getCategoryById(@PathVariable("id") Integer id) {
        Optional<Category> category = categoryService.getById(id);
        if (category.isPresent()) {
            CategoryDto categoryDto = categoryMapper.mapCategoryToCategoryDto(category.get());
            return ResponseEntity.ok(categoryDto);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/categories")
    public ResponseEntity<CategoryDto> createCategory(@RequestBody Category category) {
        Category createdCategory = categoryService.saveCategory(category);
        CategoryDto categoryDto = categoryMapper.mapCategoryToCategoryDto(createdCategory);
        return ResponseEntity.created(URI.create("/categories/" + categoryDto.getId())).body(categoryDto);
    }

    @PutMapping("/categories/{id}")
    public ResponseEntity<CategoryDto> updateCategory(@PathVariable("id") Integer id, @RequestBody Category category) {
        category.setId(id);
        Category updatedCategory = categoryService.saveCategory(category);
        if (updatedCategory != null) {
            CategoryDto categoryDto = categoryMapper.mapCategoryToCategoryDto(updatedCategory);
            return ResponseEntity.ok(categoryDto);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/categories/{id}")
    public ResponseEntity<Void> deleteCategory(@PathVariable("id") Integer id) {
        Optional<Category> category = categoryService.getById(id);
        if (category.isPresent()) {
            categoryService.deleteCategory(category.get());
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}