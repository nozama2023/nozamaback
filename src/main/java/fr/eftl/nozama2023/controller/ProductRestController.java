package fr.eftl.nozama2023.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.eftl.nozama2023.dto.ProductDto;
import fr.eftl.nozama2023.mapper.ProductMapper;
import fr.eftl.nozama2023.model.Product;
import fr.eftl.nozama2023.service.ProductService;


@RestController
public class ProductRestController {
	
	
    @Autowired
    private ProductService productService;
    
    private ProductMapper productMapper = new ProductMapper();

    @GetMapping(value = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ProductDto>> getProducts() {
        List<Product> products = productService.getProduct();
        List<ProductDto> productsDto = new ArrayList<>();
		for (Product c : products) {
			productsDto.add(productMapper.mapProductToProductDto(c));
		}
        return ResponseEntity.ok(productsDto);
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<ProductDto> getProductById(@PathVariable("id") Integer id) {
        Optional<Product> product = productService.getById(id);
        ProductDto productDto = new ProductDto();
		productDto = productMapper.mapProductToProductDto(product.get());
		
        return ResponseEntity.ok(productDto);
    }
    
    @GetMapping("/products/category/{categoryId}")
    public ResponseEntity<List<ProductDto>> getProductsByCategory(@PathVariable("categoryId") Integer categoryId) {
        List<Product> productsByCategory = productService.getProductsByCategoryId(categoryId);
        List<ProductDto> productsDto = new ArrayList<>();
        for (Product p : productsByCategory) {
            productsDto.add(productMapper.mapProductToProductDto(p));
        }
        return ResponseEntity.ok(productsDto);
    }

    @PostMapping("/products")
    public ResponseEntity<ProductDto> createProduct(@RequestBody Product product) {
        Product createdProduct = productService.saveProduct(product);
        ProductMapper productMapper = new ProductMapper();
		ProductDto productDto = productMapper.mapProductToProductDto(createdProduct);
        return ResponseEntity.ok(productDto);
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable("id") int id, @RequestBody Product product) {
        product.setId(id);
        Product updatedProduct = productService.saveProduct(product);
        if (updatedProduct != null) {
            return ResponseEntity.ok(updatedProduct);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    
	
    @DeleteMapping("/products/{id}")
	public ResponseEntity<Integer> deleteProduct(@PathVariable(value = "id") Integer id) {
		Optional<Product> optionalProduct = productService.getById(id);
		
		if (optionalProduct.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		productService.deleteProduct(optionalProduct.get());
		return ResponseEntity.noContent().build();
	}
 }
	 

	
