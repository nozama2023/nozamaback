package fr.eftl.nozama2023.controller;

import java.awt.PageAttributes.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.eftl.nozama2023.dto.GrantDto;
import fr.eftl.nozama2023.mapper.GrantMapper;
import fr.eftl.nozama2023.model.Grant;
import fr.eftl.nozama2023.service.GrantService;

@RestController
public class GrantRestController {
	
	@Autowired
	private GrantService grantService;
	
	private GrantMapper grantMapper = new GrantMapper();
	
	@GetMapping(value = "/grants")
	public ResponseEntity<List<GrantDto>> getGrants() {
		List<Grant> grants = grantService.getGrant();
		List<GrantDto> grantsDto = new ArrayList<>();
		for (Grant c : grants) {
			grantsDto.add(grantMapper.mapGrantToGrantDto(c));
		}
		return ResponseEntity.ok(grantsDto);
		
	}
	
	@GetMapping("/grants/{id}")
	public ResponseEntity<GrantDto> getGrantById(@PathVariable("id") Integer id) {
		Optional<Grant> grant = grantService.getGrantById(id);
		GrantDto grantDto = new GrantDto();
		grantDto = grantMapper.mapGrantToGrantDto(grant.get());
		
		return ResponseEntity.ok(grantDto);	
	}
	
	@PostMapping("/grants")
    public ResponseEntity<GrantDto> createGrant(@RequestBody Grant grant) {
        Grant createdGrant = grantService.saveGrant(grant);
        GrantMapper grantMapper = new GrantMapper();
		GrantDto grantDto = grantMapper.mapGrantToGrantDto(createdGrant);
        return ResponseEntity.ok(grantDto);
    }

    @PutMapping("/grants/{id}")
    public ResponseEntity<Grant> updateGrant(@PathVariable("id") int id, @RequestBody Grant grant) {
        grant.setId(id);
        Grant updatedGrant = grantService.saveGrant(grant);
        if (updatedGrant != null) {
            return ResponseEntity.ok(updatedGrant);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    
	
    @DeleteMapping("/grants/{id}")
	public ResponseEntity<Integer> deleteGrant(@PathVariable(value = "id") Integer id) {
		Optional<Grant> optionalGrant = grantService.getGrantById(id);
		
		if (optionalGrant.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		grantService.deleteGrant(optionalGrant.get());
		return ResponseEntity.noContent().build();
	}
	

}
