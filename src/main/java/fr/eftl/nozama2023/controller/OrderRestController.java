package fr.eftl.nozama2023.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.eftl.nozama2023.dao.OrderDao;
import fr.eftl.nozama2023.dto.OrderDto;
import fr.eftl.nozama2023.form.OrderForm;
import fr.eftl.nozama2023.mapper.OrderMapper;
import fr.eftl.nozama2023.model.Order;
import jakarta.validation.Valid;

@RestController
public class OrderRestController {

    @Autowired
    private OrderDao orderDao;

    // Find list of orders
    @GetMapping(value = "/orders", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<OrderDto>> orders() {
        List<OrderDto> orderListDto = new ArrayList<>();
        OrderMapper orderMapper = new OrderMapper();
        List<Order> orders = orderDao.findAll();
        for (Order order : orders) {
            OrderDto orderDto = orderMapper.mapOrderToOrderDto(order);
            orderListDto.add(orderDto);
        }
        return ResponseEntity.ok().body(orderListDto);
    }

    // Find order with id
    @GetMapping("/orders/{id}")
    public ResponseEntity<OrderDto> orderById(@PathVariable(value = "id") Integer id) {
        Optional<Order> order = orderDao.findById(id);
        OrderMapper orderMapper = new OrderMapper();
        if (order.isPresent()) {
            OrderDto orderDto = orderMapper.mapOrderToOrderDto(order.get());
            return ResponseEntity.ok().body(orderDto);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    // Create an order
    @PostMapping("/orders")
    public ResponseEntity<OrderDto> createOrder(@Valid @RequestBody final OrderForm orderForm) {
        Order order = new Order();
        order.setStatus(orderForm.getStatus());
        order.setDate(orderForm.getDate());
        order.setPrice(orderForm.getPrice());

        Order newOrder = orderDao.save(order);

        OrderMapper orderMapper = new OrderMapper();
        OrderDto orderDto = orderMapper.mapOrderToOrderDto(newOrder);

        return ResponseEntity.created(URI.create("/orders/" + newOrder.getId())).body(orderDto);
    }

    // Delete an order
    @DeleteMapping("/orders/{id}")
    public ResponseEntity<OrderDto> deleteOrder(@PathVariable(value = "id") Integer id) {
        Optional<Order> order = orderDao.findById(id);
        if (order.isEmpty()) {
            return ResponseEntity.notFound().build();
        } else {
            orderDao.delete(order.get());
            return ResponseEntity.noContent().build();
        }
    }

    // Update an order
    @PutMapping("/orders/{id}")
    public ResponseEntity<OrderDto> updateOrder(@PathVariable(value = "id") Integer id,
            @Valid @RequestBody final OrderForm orderForm) {
        Optional<Order> order = orderDao.findById(id);
        if (order.isPresent()) {
            order.get().setStatus(orderForm.getStatus());
            order.get().setDate(orderForm.getDate());
            order.get().setPrice(orderForm.getPrice());

            orderDao.save(order.get());

            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
