package fr.eftl.nozama2023.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eftl.nozama2023.model.Contact;

public interface ContactDao extends JpaRepository<Contact, Integer> {

}
