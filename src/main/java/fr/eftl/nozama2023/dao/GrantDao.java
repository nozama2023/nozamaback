package fr.eftl.nozama2023.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eftl.nozama2023.model.Grant;

public interface GrantDao extends JpaRepository<Grant, Integer> {

}
