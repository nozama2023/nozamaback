package fr.eftl.nozama2023.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eftl.nozama2023.model.Order;

public interface OrderDao extends JpaRepository<Order, Integer> {
	
	List<Order> findByUserId(Integer userId);

	List<Order> findByStatus(String orderStatus);

	List<Order> findByDate(String orderDate);
}
