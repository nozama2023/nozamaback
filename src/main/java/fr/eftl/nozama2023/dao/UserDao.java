package fr.eftl.nozama2023.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eftl.nozama2023.model.User;

public interface UserDao extends JpaRepository<User, Integer> {

	
	List<User> findByLastname(String lastname);
	
	List<User> findByFirstname(String firstname);
	
	List<User> findByMail(String mail);
	

	
}




//	User findByLastname(String lastname);
	
	/**
	 * Get a list of all users
	 * 
	 * @return the list of all users
	 * @throws DaoException
	 */
