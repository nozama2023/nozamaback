package fr.eftl.nozama2023.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eftl.nozama2023.model.Category;
import fr.eftl.nozama2023.model.Product;

public interface ProductDao extends JpaRepository<Product, Integer> {
	
	List<Product> findByCategoryId(Integer categoryId);

	List<Product> findByName(String productName);

	List<Product> findByCategory(Category category1);

	Product findByReference(String productReference);

}
