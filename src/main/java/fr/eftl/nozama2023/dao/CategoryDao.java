package fr.eftl.nozama2023.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eftl.nozama2023.model.Category;

public interface CategoryDao extends JpaRepository<Category, Integer> {

	List<Category> findByName(String categoryNames);

}
