package fr.eftl.nozama2023.form;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class CategoryForm {
	
	@NotNull
	@Size(min = 2, max = 100)
	private String name;
	
	@NotNull
	@Size(min = 3, max = 255)
	private String description;

	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String toString() {
		return "Product(Name: " + this.name + ", Description: " + this.description + ")";
	}
}
