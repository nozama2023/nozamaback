package fr.eftl.nozama2023.form;

import jakarta.validation.constraints.NotNull;

import jakarta.validation.constraints.Size;

import java.util.Date;

public class OrderForm {
    @NotNull
    private Integer id;

    @NotNull
    private Integer idUser;

    @NotNull
    @Size(min = 1, max = 100)
    private String status;

    @NotNull
    private String date;

    @NotNull
    private Double price;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
    
    @Override
    public String toString() {
        return "OrderForm{" +
                "id=" + id +
                ", idUser=" + idUser +
                ", status='" + status + '\'' +
                ", date=" + date +
                ", price=" + price +
                '}';
    }




}
