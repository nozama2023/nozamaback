package fr.eftl.nozama2023.form;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class UserForm {
	@NotNull
	@Size(min = 2, max = 100)
	private Integer id;

	@NotNull
	@Size(min = 2, max = 200)
	private String lastname;

	@NotNull
	@Email
	@Size(min = 3, max = 255)
	private String firstname;

	@NotNull(message = "civilité est obligatoire")
	@Size(min = 10, max = 15)
	private String gender;

	@NotNull
	@Size(min = 10, max = 15)
	private Integer phone;

	@Size(max = 255)
	private String mail;

	@NotNull
	private String birthdate;
	
	@NotNull
	private String password;
	
	@NotNull
	private Integer roleFk;
	
	@NotNull
	private String signinDate;

	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getPhone() {
		return phone;
	}

	public void setPhone(Integer phone) {
		this.phone = phone;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public Integer getRoleFk() {
		return roleFk;
	}

	public void setRoleFk(Integer roleFk) {
		this.roleFk = roleFk;
	}
	
	public String getSigninDate() {
		return signinDate;
	}

	public void setSigninDate(String signinDate) {
		this.signinDate = signinDate;
	}

	public String toString() {
		return "User [id=" + id + ", lastname=" + lastname + ", firstname=" + firstname + ", gender=" + gender
				+ ", phone=" + phone + ", mail=" + mail + ", birthdate=" + birthdate + ", password=" + password
				+ ", roleFk=" + roleFk + ", signinDate=" + signinDate + "]";
	}
}
