package fr.eftl.nozama2023.form;


import fr.eftl.nozama2023.model.Category; 


import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;


public class ProductForm {
	@NotNull
	@Size(min = 2, max = 100)
	private String name;

	@NotNull
	@Size(min = 2, max = 200)
	private String reference;

	@NotNull
	@Size(min = 3, max = 255)
	private String description;

	@NotNull(message = "quantity est obligatoire")
	@Size(min = 10, max = 15)
	private Integer quantity;

	@NotNull
	@Size(min = 10, max = 15)
	private Double price;

	@Size(max = 255)
	private Category category;

	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantityStock(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	

	public String toString() {
		return "Product(Name: " + this.name + ", Reference: " + this.reference + ", Description: " + this.description
				+ ", Quantity in Stock: " + this.quantity + ", Price: " + this.price + ", Category: " + this.category +")";
	}
}
