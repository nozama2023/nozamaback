package fr.eftl.nozama2023.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import fr.eftl.nozama2023.config.MyUserDetails;
import fr.eftl.nozama2023.dao.UserDao;
import fr.eftl.nozama2023.model.User;

public class MyUserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserDao userDao;

	public UserDetails loadUserByUsername(String lastname) throws UsernameNotFoundException {
		List<User> user = userDao.findByLastname(lastname);

		if (null == user) {
			throw new UsernameNotFoundException("User with username " + lastname + " not found !");
		}

		return new MyUserDetails(user);
	}

}