package fr.eftl.nozama2023;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


@SpringBootApplication
public class Nozama2023Application extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(Nozama2023Application.class, args);
	}

	
}
