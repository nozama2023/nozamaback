package fr.eftl.nozama2023.model;

import java.io.Serializable;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table (name = "product")
public class Product implements Serializable {

	private static final long serialVersionUID = 5800032885120452660L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Product_id") 
	private Integer id;
	
	@Column(name = "Product_reference", length = 100)
	private String reference;
	
	@Column(name = "Product_name",length = 100)
	private String name;
	
	@Column(name = "Product_description",length = 100)
	private String description;

	@Column(name = "Product_quantity")
	private Integer quantity;

	@Column(name = "Product_price", columnDefinition = "DECIMAL")
	private Double price;
	
	

	 
	 @ManyToOne(fetch = FetchType.LAZY)
	 @JoinColumn(name = "Category_id") 
	 
	private Category category; 
	 
	public Product() {
		super();
	}
	
	public Product(Integer id, String reference, String name, String description, 
			Integer quantity, Double price, Category category) {
		this.id = id;
		this.reference = reference;
		this.name = name;
		this.description = description;
		this.quantity = quantity;
		this.price = price;
		this.category = category;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", reference=" + reference + ", name=" + name + ", description=" + description
				+ ", quantityStock=" + quantity + ", price=" + price + ", category=" + category + "]";
	}
	
	
	

}
