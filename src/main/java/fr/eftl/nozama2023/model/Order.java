 package fr.eftl.nozama2023.model;

import java.io.Serializable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "orders")
public class Order implements Serializable {


	private static final long serialVersionUID = 8964180194246107607L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name= "Order_id")
    private Integer id;

	/*
		 * Column(name = "user_id") private Integer idUser;
		 */

    @Column(name = "Order_status", length = 100)
    private String status;

    @Column(name = "Order_date", length = 100)
    private String date;

    @Column(name = "Order_price", length = 100)
    private Double price;

    
    @ManyToOne(fetch = FetchType.LAZY)
	 @JoinColumn(name = "User_id") 
    private User user; 
 
    public Order() {
        super();
    }

    public Order(Integer id, Integer idUser, String status, String date, Double price, User user) {
        this.id = id;
        this.status = status;
        this.date = date;
        this.price = price;
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

   

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
    
    public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "Product [id=" + id + ", status=" + status + ", date=" + date
				+ ", price=" + price+ ", user=" + user + "]";
	}


	}

