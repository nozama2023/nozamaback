package fr.eftl.nozama2023.model;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table (name = "adress")
public class Adress implements Serializable {

	
	private static final long serialVersionUID = 8657666826839244751L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "Adress_name", length = 100)
	private String lastname;
	
	@Column(name = "Adress_firstname", length = 100)
	private String firstname;
	
	@Column(name = "Adresse_street_number", length = 100)
	private String streetNumber;
	
	@Column(name = "Adress_street", length = 100)
	private String street;
	
	@Column(name = "Adress_infos", length = 100)
	private String infos;
	
	@Column(name = "Adresse_postcode", length = 100)
	private Integer postcode;
	
	@Column(name = "Adress_city", length = 100)
	private String city;
	
	@Column(name = "Adresse_country", length = 100)
	private String country;
	
	public Adress() {
		super();
	}
	
	public Adress(Integer id, String lastname, String firstname, String streetNumber,
			String street, String infos, Integer postcode, String city, String country) {
		this.id = id;
		this.lastname = lastname;
		this.firstname = firstname;
		this.streetNumber = streetNumber;
		this.street = street;
		this.infos = infos;
		this.postcode = postcode;
		this.city = city;
		this.country = country;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getInfos() {
		return infos;
	}

	public void setInfos(String infos) {
		this.infos = infos;
	}

	public Integer getPostcode() {
		return postcode;
	}

	public void setPostcode(Integer postcode) {
		this.postcode = postcode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "Adress [id=" + id + ", lastname=" + lastname + ", firstname=" + firstname + ", street_number="
				+ streetNumber + ", street=" + street + ", infos=" + infos + ", postcode=" + postcode + ", city=" + city
				+ ", country=" + country + "]";
	}

	
	
}
