package fr.eftl.nozama2023.model;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table (name = "comment")
public class Comment implements Serializable {


	private static final long serialVersionUID = 8754514139748573580L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "Comment_grade", length = 100)
	private Integer grade;
	
	@Column(name = "Comment_text", columnDefinition = "TEXT")
	private String commentary;
	
	@Column(name = "Avis_Product_FK", length = 100)
	private Integer productFk;
	
	public Comment() {
		super();
	}
	
	public Comment(Integer id, Integer note, String commentary, Integer productFk) {
		this.id = id;
		this.grade = note;
		this.commentary = commentary;
		this.productFk = productFk;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNote() {
		return grade;
	}

	public void setNote(Integer note) {
		this.grade = note;
	}

	public String getCommentary() {
		return commentary;
	}

	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}

	public Integer getProductFk() {
		return productFk;
	}

	public void setProductFk(Integer productFk) {
		this.productFk = productFk;
	}

	@Override
	public String toString() {
		return "comment [id=" + id + ", note=" + grade + ", commentary=" + commentary + ", productFk=" + productFk + "]";
	}
	

}
