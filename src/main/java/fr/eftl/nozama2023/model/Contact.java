package fr.eftl.nozama2023.model;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table (name = "contact")
public class Contact implements Serializable{

	
	private static final long serialVersionUID = 7385300784305969789L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "Contact_mail", length = 100)
	private String mail;
	
	@Column(name = "Contact_phone", length = 100)
	private String phone;
	
	@Column(name = "Contact_fax", length = 100)
	private String fax;
	
	@Column(name = "Contact_mobile", length = 100)
	private String mobile;
	
	public Contact() {
		super();
	}
	
	public Contact(Integer id, String mail, String phone, String fax, String mobile) {
		this.id = id;
		this.mail = mail;
		this.phone = phone;
		this.fax = fax;
		this.mobile = mobile;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getFax() {
		return fax;
	}
	
	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Override
	public String toString() {
		return "Contact [id=" + id + ", mail=" + mail + ", phone=" + phone + ", fax=" + fax + ", mobile=" + mobile
				+ "]";
	}


	
	

}
