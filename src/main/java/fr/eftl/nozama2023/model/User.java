package fr.eftl.nozama2023.model;

import java.io.Serializable;

import org.hibernate.annotations.ManyToAny;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table (name = "users")
public class User implements Serializable {

	
	private static final long serialVersionUID = -7478871433325515535L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "User_id")
	private Integer id;
	
	@Column(name = "User_name", length = 100)
	private String lastname;
	
	@Column(name = "User_firstname", length = 100)
	private String firstname;
	
	@Column(name = "User_gender", length = 100)
	private String gender;
	
	@Column(name = "User_mobile", length = 10)
	private Integer phone;
	
	@Column(name = "User_mail", length = 100)
	private String mail;
	
	@Column(name = "User_birth_date", columnDefinition = "DATE")
	private String birthdate;
	
	@Column(name = "User_password", length = 100)
	private String password;
	
	@Column(name = "Role_id", length = 100)
	private Integer role;
	
	@Column(name = "User_sign_in_date", columnDefinition = "DATE")
	private String signinDate;
	
	@Column(length = 255)
	private String grants;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Grant_id")
	
	private Grant grant;
	
	public User() {
		super();
	}
	
	public User(Integer id, String lastname, String firstname, String gender, Integer phone,
			String mail, String birthdate, String password, Integer role, String signinDate, Grant grant) {
		this.id = id;
		this.lastname = lastname;
		this.firstname = firstname;
		this.gender = gender;
		this.phone = phone;
		this.mail = mail;
		this.birthdate = birthdate;
		this.password = password;
		this.role = role;
		this.signinDate = signinDate;
		this.grant = grant;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getPhone() {
		return phone;
	}

	public void setPhone(Integer phone) {
		this.phone = phone;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	public String getSigninDate() {
		return signinDate;
	}

	public void setSigninDate(String signinDate) {
		this.signinDate = signinDate;
	}

	public String getGrants() {
		return grants;
	}
	
	public void setGrants(Grant grant) {
		this.grant = grant;
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", lastname=" + lastname + ", firstname=" + firstname + ", gender=" + gender
				+ ", phone=" + phone + ", mail=" + mail + ", birthdate=" + birthdate + ", password=" + password
				+ ", role=" + role + ", signinDate=" + signinDate +", grant=" + grant + "]";
	}

	
	
	
}