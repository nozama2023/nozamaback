package fr.eftl.nozama2023.model;

import java.io.Serializable;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table (name = "grant")
public class Grant implements Serializable {

		
	
	private static final long serialVersionUID = -2611004858352002592L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Grant_id")
	private Integer id;
	
	@Column(name = "Grant_name", length = 100)
	private String name;

	@OneToMany(mappedBy = "grant", cascade = CascadeType.ALL)
	private List<User> users;
	
	public Grant() {
		super();
	}
	
	public Grant(Integer id, String name) {
		this.id = id;
		this.name = name;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	@Override
	public String toString() {
		return "Grant [id=" + id + ", name=" + name + ", users=" + users + "]";
	}
	
	

}


