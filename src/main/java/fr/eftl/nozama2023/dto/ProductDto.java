package fr.eftl.nozama2023.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import fr.eftl.nozama2023.model.Category;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductDto {
	private Integer id;

	private String name;

	private String reference;

	private String description;

	private Integer quantity;

	private Double price;

	private Category category;
	

	

	public ProductDto() {
		
	}

	public ProductDto(Integer id, String name, String reference, String description, 
			Integer quantity, Double price, Category category) {
		super();
		this.id = id;
		this.name = name;
		this.reference = reference;
		this.description = description;
		this.quantity = quantity;
		this.price = price;
		this.category = category;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@JsonIgnore
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

}
