package fr.eftl.nozama2023.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto {
	
	private Integer id;

	private String lastname;

	private String firstname;

	private String gender;

	private Integer phone;

	private String mail;

	private String birthdate;
	
	private String password;
	
	private Integer roleFk;
	
	private String signinDate;
	

	

	public UserDto() {
		
	}

	public UserDto(Integer id, String lastname, String firstname, String gender, Integer phone,
			String mail, String birthdate, String password, Integer roleFk, String signinDate) {
		super();
		this.id = id;
		this.lastname = lastname;
		this.firstname = firstname;
		this.gender = gender;
		this.phone = phone;
		this.mail = mail;
		this.birthdate = birthdate;
		this.password = password;
		this.roleFk = roleFk;
		this.signinDate = signinDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getPhone() {
		return phone;
	}

	public void setPhone(Integer phone) {
		this.phone = phone;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getRoleFk() {
		return roleFk;
	}

	public void setRoleFk(Integer roleFk) {
		this.roleFk = roleFk;
	}

	public String getSigninDate() {
		return signinDate;
	}

	public void setSigninDate(String signinDate) {
		this.signinDate = signinDate;
	}

}
