package fr.eftl.nozama2023.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GrantDto {
	
	private Integer id;
	
	private String name;
	
	public GrantDto() {
		
	}
	
	public GrantDto(Integer id, String name) {
		this.id = id;
		this.name = name;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "GrantDto [id=" + id + ", name=" + name + "]";
	}
	

}
