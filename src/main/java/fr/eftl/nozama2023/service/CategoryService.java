package fr.eftl.nozama2023.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eftl.nozama2023.dao.CategoryDao;
import fr.eftl.nozama2023.model.Category;

@Service
public class CategoryService {
	
	@Autowired
	private CategoryDao categoryDao;

	public CategoryService(CategoryDao categoryDao) {
		this.categoryDao = categoryDao;
	}

	public List<Category> getCategory(){
		return categoryDao.findAll();
	}

	public Category saveCategory(Category category) {
		return categoryDao.save(category);
	}

	public void deleteCategory(Category category) {
		categoryDao.delete(category);
	}
	
	public Optional<Category> getById(int id) {
		return categoryDao.findById(id);
	}
}
