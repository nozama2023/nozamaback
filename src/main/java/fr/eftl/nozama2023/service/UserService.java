package fr.eftl.nozama2023.service;

import java.util.List;
import java.util.Optional;

import fr.eftl.nozama2023.dao.UserDao;
import fr.eftl.nozama2023.model.User;

public class UserService {
	private UserDao userDao;

	public UserService(UserDao userDao) {
		this.userDao = userDao;
	}

	public List<User> getUser(){
		return userDao.findAll();
	}

	public User saveUser(User user) {
		return userDao.save(user);
	}

	public void deleteUser(User user) {
		userDao.delete(user);
	}
	
	public Optional<User> getById(int id) {
		return userDao.findById(id);
	}
}
