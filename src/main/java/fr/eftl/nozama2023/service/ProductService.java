package fr.eftl.nozama2023.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import fr.eftl.nozama2023.dao.ProductDao;
import fr.eftl.nozama2023.model.Category;
import fr.eftl.nozama2023.model.Product;

@Service
public class ProductService {


private ProductDao productDao;

@Autowired
public ProductService(ProductDao productDao) {
	this.productDao = productDao;
}

public List<Product> getProduct(){
	return productDao.findAll();
}

public List<Product> getProductsByCategoryId(Integer categoryId) {
    return productDao.findByCategoryId(categoryId);
}

public Product saveProduct(Product product) {
	return productDao.save(product);
}

public void deleteProduct(Product product) {
	productDao.delete(product);
	
}

public Optional<Product> getById(Integer id) {
	return productDao.findById(id);
}
}
