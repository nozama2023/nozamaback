package fr.eftl.nozama2023.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eftl.nozama2023.dao.GrantDao;
import fr.eftl.nozama2023.model.Grant;

@Service
public class GrantService {
	
	private GrantDao grantDao;
	
	@Autowired
	public GrantService(GrantDao grantDao) {
		this.grantDao = grantDao;
	}
	
	public List<Grant> getGrant() {
		return grantDao.findAll();
	}
	
	public Optional<Grant> getGrantById(Integer id) {
		return grantDao.findById(id);
	}
	
	public Grant saveGrant(Grant grant) {
		return grantDao.save(grant);
	}
	
	public void deleteGrant(Grant grant) {
		grantDao.delete(grant);
		
	}
	

}
