package fr.eftl.nozama2023.service;

import java.util.List;
import java.util.Optional;

import fr.eftl.nozama2023.dao.OrderDao;
import fr.eftl.nozama2023.model.Order;

public class OrderService {
	private OrderDao orderDao;

	public OrderService(OrderDao orderDao) {
		this.orderDao = orderDao;
	}

	public List<Order> getOrder(){
		return orderDao.findAll();
	}

	public Order saveOrder(Order order) {
		return orderDao.save(order);
	}

	public void deleteOrder(Order order) {
		orderDao.delete(order);
	}
	
	public Optional<Order> getById(int id) {
		return orderDao.findById(id);
	}
}
